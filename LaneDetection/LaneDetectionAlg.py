from sklearn.cluster import MiniBatchKMeans
import cv2

# http://www.pyimagesearch.com/2014/07/07/color-quantization-opencv-using-k-means-clustering/	
def ColorSegment(image, clusters):	
		
	# convert the image from the RGB color space to the L*a*b*
	# color space -- since we will be clustering using k-means
	# which is based on the euclidean distance, we'll use the
	# L*a*b* color space where the euclidean distance implies
	# perceptual meaning
	imageSeg = cv2.cvtColor(image, cv2.COLOR_RGB2LAB)
		
	# reshape the image into a feature vector so that k-means can be applied
	imageSeg = imageSeg.reshape((imageSeg.shape[0] * imageSeg.shape[1], 3))
	
	# apply k-means using the specified number of clusters and
	# then create the quantized image based on the predictions
	clt = MiniBatchKMeans(n_clusters = clusters)
	labels = clt.fit_predict(imageSeg)
	imageSeg = clt.cluster_centers_.astype("uint8")[labels]

	# reshape the feature vectors to images
	imageSeg = imageSeg.reshape(image.shape)
 
	# convert from L*a*b* to RGB
	imageSeg = cv2.cvtColor(imageSeg, cv2.COLOR_LAB2RGB)
	
	return imageSeg

def segMask(segmentedImage, color):
		
	return cv2.inRange(segmentedImage, color, color)
	
