from tkinter import *
from OpenCVCanvas import OpenCVCanvas

# Add the ability to mark a mouse click (with a blue 'X') to our OpenCV image display canvas
class ClickableOpenCVCanvas(OpenCVCanvas):
	
	def __init__(self, args, **kwargs):
		super().__init__(args, **kwargs)
		
		# mouse click binding
		self.bind("<Button-1>", self.mouseClick)
		
		# Display characteristic of the blue 'X' - size, color, thickness
		self.radius = 12
		self.color = 'cyan'
		self.width = 2
		
		# Positional characteristics of the 'X'
		self.clickPoint = None
		self.cross = []
		
		# Event callbacks
		self.onClick = None
		self.clearClick = None
		
	# Register the callbacks
	def setCallbacks(self, onClick, clearClick):
		self.onClick = onClick
		self.clearClick = clearClick
	
	# Got a Click!
	def mouseClick(self, event):
		# Make sure the click is within the image on the screen
		if self.img is not None and event.x <= self.img.shape[1] and event.y <= self.img.shape[0]:
			# register the click coords
			self.clickPoint = (event.x, event.y)
			# clear the old 'X'
			[self.delete(x) for x in self.cross]
			# Draw the new 'X' as two lines stored in a list
			self.cross = [self.create_line(event.x-self.radius/2, event.y-self.radius/2, event.x+self.radius/2, 
										   event.y+self.radius/2, fill=self.color, width=self.width),
						  self.create_line(event.x-self.radius/2, event.y+self.radius/2, event.x+self.radius/2, 
									       event.y-self.radius/2, fill=self.color, width=self.width)]
			# Fire the click callback
			if self.onClick is not None:
				self.onClick(event)
				
	# Clear the 'X' when we load a new image
	def loadImage(self):
		# Standard OpenCVCanvas loadImage()
		loaded = super().loadImage()
		if loaded:
			# clear the 'X'
			self.clickPoint = None
			[self.delete(x) for x in self.cross]
			self.cross = []
			# Fire the clear callback
			if self.clearClick is not None:
				self.clearClick()
			return True
		else:
			return False

