#! /usr/bin/python

if __name__ == '__main__':
    from EXbee import EXbee
    kk = EXbee('COM6', 9600)

    response = kk.read_rx()

    print("Sender:%s     " % response['SOURCE_ADDRESS_64'])
    print("Message: %s   " % response['DATA'])

    print(response)
