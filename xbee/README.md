## Configuring XBee modules for our purpose.

We are supposed to use Windows instead of MAC OSx or Linux.

### Download and install XCTU.

1. Download XCTU from http://www.digi.com/products/xbee-rf-solutions/xctu-software/xctu and install on your PC.

2. Once X-CTU has been downloaded, the next step is to install the program. 

    When the program asks for updating from Digi, we must answer ‘yes’ so as to download all the firmware versions for all the XBee modules.

3. Connect your XBee module with **XBee USB explorer dongle**. 
    
    https://www.sparkfun.com/products/11697
    or 
    https://www.sparkfun.com/products/11812
    
4. Plug the XBee Pro 3B module into the XBee Explorer, attach a mini USB cable, and you will have direct access to the serial and programming pins on the XBee unit.

## Configure XCTU.

When X-CTU has been properly installed, the XBee USB explorer dongles should be connected to the computer. 

Those will be recognized as **USB Serial Port**. 

We have to know the COM number given to this device in order to specify it in the X-CTU (in our test, COM1 was the value given by Windows).

Finally, we launch X-CTU and the program will start. A window like the one below will appear, showing the different functions and the different COM ports detected.

![Startup screen](img/1.jpg "Startup screen")

1. Discover modules.

    Press **+** button at the left top corner.
    
    ![Discovering XBee](img/2.jpg "Discovering XBee")
    
    Since we are using Pro 3B programmable module, we need to check **The radio module is programmable**.
    
    Once check the checkbox, select proper COM port of your module and press **Finish** button.
    
    NOTE: Please ensure that you have selected COM port properly.
    
    ![Select COM port correctly](img/3.jpg "Select COM port correctly")
    
    As you can see in the image above, COM port must be highlighted with light blue color.
    
    Otherwise XCTU will fail to discover device. (It is just bug of XCTU and I had wasted about 2 hours to fix this!)
    
    After discovering 2 modules, screen will look like this:
    
    ![Initial discovering](img/4.jpg "Initial discovering")
    
2. Configure XBee modules.

    Now we need to configure XBee modules for point-to-multipoint mode.
    
    In default XBee modules are configured as *Transparent Mode*.
    
    It is suitable with point-to-point network, but is not capable with our case since we need point-to-multipoint mode.
    
    So let us configure XBee modules as API mode.
        
- Coordinator.
 
    Select 1st module and change some settings at the right side of window.
  
        ID     Network ID       1234
    
    After changing value, press `write` button to apply changes.
    
    (*NOTE* : Every modules should have same Network ID. I just used 1234 as sample value and you could change it as you like.)

    ![Changing Network ID](img/5.jpg "Changing Network ID")
    
    Now Change the Network setting.
    
        CE     Routing/Messaging Mode       Indirect Msg Coordinator[1]
        
    ![Changing Network Mode](img/6.jpg "Changing Network Mode")
    
    Enable API mode.
    
        AP     API Enable       API Mode With Escapes[2]
        
    ![Enable API Mode](img/7.jpg "Enable API mode")
    
    **Now set some additional parameters.**
    
        NI      Node Identifier                 Master(Give any name as you like)
        NO      Network Discovery Options       5
        
- Standard Router
    
    **Exactly* Same with Coordinator except Network setting.
    
        CE     Routing/Messaging Mode           Standard Router[0]
        
    ![Changing Network Mode](img/8.jpg "Changing Network Mode")
    
    And **NI** value should be the name of Standard Router.
    
- Disconnect XBee modules and plug again and discover.
   
    You can see that those icons on the left side are changed.
    
    ![New configuration](img/9.jpg "New configuration")
    
*NOTE:* In the pictures above, the configuration of 2nd device is wrong and it should be Standard Router instead of End Device.
    
3. Connnect XBee modules each other and test communication.

- Connect XBee modules.

    Switch to **Network Working Mode** by pressing network icon at the right top corner.
    
    After start scanning on both of XBee modules, you can see those are connected like below:
    
    ![Connecting Devices](img/10.jpg "Connecting Devices")
    
- Test communication.

    You can try to test communication by following http://www.digi.com/resources/documentation/Digidocs/90002127/tasks/t_send_an_api_frame_from_the_third_xbee_module_to_other_modules.htm
    
    *NOTE*: In the tutorial above, they used transparent mode that they could see received text as ASCII code.
     
     But in our case we could see only HEX values since we are using API mode.
     
    
    
    
    




































    