# Copyright ConstrucTRON Inc, 2016

# import the necessary packages
import time
import cv2
import numpy as np
import sys

# simple struct to represent a vehicle's position
class Vehicle:
    def __init__(self, x, y):
        self.xPos = x
        self.yPos = y
    
    # Euclidean distance
    def dist(self, other):
        return np.linalg.norm(np.array([self.xPos, self.yPos])-np.array([other.xPos, other.yPos]))
    
    # Faster distance function - rectangular distance  
    def rDist(self, other):
        return abs(self.xPos - other.xPos) + abs(self.yPos - other.yPos)
        
# An subwindow/ROI
class WindowBox:

	def __init__(self, t, b, l, r, color):
		self.top = t
		self.bottom = b
		self.left = l
		self.right = r
		self.color = color
		self.vehicles_old_frames = []
		
	def getSubimg(self, img):
		return img[self.top:self.bottom,self.left:self.right]
	
	def publishToWindow(self, img, scene):
		img[self.top:self.bottom,self.left:self.right] = scene
	
	def drawOpenCVRect(self, img):
		cv2.rectangle(img, (self.left, self.top), (self.right, self.bottom), self.color, 7)

# Setup the Background Subtractor
# MOG1 no longer exists - http://code.opencv.org/issues/4367
pMOG = cv2.createBackgroundSubtractorMOG2()
# Build the erosion and dialation elements
erodeElement = cv2.getStructuringElement(cv2.MORPH_RECT, (4,4))
# These should move with changes in resolution (pixels/vehicle)
dilateElement = cv2.getStructuringElement(cv2.MORPH_RECT, (4,4))

# Minimum threshold for motion processing 
SENSITIVITY_VALUE = 20
# If number of objects in a frame is greater than MAX_NUM_OBJECTS we have a noisy filter
MAX_NUM_OBJECTS = 50
# Smallest acceptable contour (noise threshold)
MIN_OBJECT_AREA = 100

# When counting entrants into a box, how tall should the entrant window be?
TOP_BUFFER = 40
# When counting entrants into a box, how far away does the centroid have to be to count as a new vehicle?
MIN_NEW_VEHICLE_DISTANCE = 20
# how many frames back should we check vehicle history?
VEHICLE_HISTORY_LOOKBACK = 2

# Try and isolate vehicles as white blobs
def processFrame(frame):
    
    # Blur filter - reduce noise
    #blurred = cv2.blur(foregroundMask, (20,20))

    # shrink morph
    eroded = cv2.erode(frame, erodeElement)

    # grow morph
    dilated = cv2.dilate(eroded, dilateElement)

	# shrink/grow is a good way to get rid of noise, without softening edges like a filter

    # threshold - get rid of pixels with near-zero values
    thresholded = cv2.threshold(eroded, SENSITIVITY_VALUE, 255, cv2.THRESH_BINARY)[1]
    
    return thresholded

# Using contour to turn the white blobs into shapes - we'll track these shapes by their centroid
def searchForMovement(processedFrame):
    # find contours screws with the input matrix, so make a copy
    temp = np.copy(processedFrame)

    # find the contours (shapes) in the processed image
    _, contours, hierarchy = cv2.findContours(temp, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # initialize return list
    vehicles = []

    # This is a really weird way of iterating through the contours.  Especially since RETR_EXTERNAL
    #   removes all nested contours
    if hierarchy is not None and hierarchy.shape[1] <= MAX_NUM_OBJECTS:
        index = 0
        while index >= 0:

            # ??? what about contourArea() ?
            moment = cv2.moments(contours[index])
            area = moment['m00']

            # throw out small contours as noise
            if area > MIN_OBJECT_AREA:
                vehicle = Vehicle(moment['m10']/area, moment['m01']/area)
                vehicles.append(vehicle)

            # goto next contour in hierarchy
            index = hierarchy[0][index][0]

    return vehicles

# When does a vehicle enter the box?  (from the top)
def trackVehicles(vehicles_old_frames, vehicles_new):
    count = 0
    for vehicle in vehicles_new: 
        # If it just crosses a line, noise and sample size will cause misses.
        # So let's use a stripe of height "TOP_BUFFER" as our trigger "line"
        # If we find a vehicle centeroid in the TOP_BUFFER, it's an entrant       
        if vehicle.yPos < TOP_BUFFER:
            for frame in vehicles_old_frames:                
                for vehicle_old in frame:
                    # However, a vehicle might get counted twice if it doesn't leave the buffer stripe
                    # area before the next frame.  So we track the last few frames, and if the vehicle is too close
                    # to a vehicle from a recent frame, consider it a potential double-count and ignore it
                    if vehicle.rDist(vehicle_old) < MIN_NEW_VEHICLE_DISTANCE:
                        break
                else:
                    continue
                break
            else:                    
                count += 1
    return count

# main MOG function - give me a picture a list of subwindow, and a display flag
def doMOG(img, window_boxes, showProcessFlag):		
    
    # Background subtraction
    foregroundMask = pMOG.apply(img);
    # Can't double up the MOG (i.e. can't apply twice on separate windows
    # if this gets hairy, we could min/max all the subwindows and only MOG that
    
    entrants = []
    
    for window_box in window_boxes:
        window = window_box.getSubimg(foregroundMask)
        
        # Process the frame
        processed = processFrame(window)
        
        # Look for the vehicles
        results = searchForMovement(processed)	
        
        # debug mode
        if showProcessFlag:
            # for now, just draw circles
            processedColor = cv2.cvtColor(processed, cv2.COLOR_GRAY2RGB)# need color
            for vehicle in results:
                cv2.circle(processedColor, (int(vehicle.xPos), int(vehicle.yPos)), 10, (0, 255, 0, 255), -1)        
            window_box.publishToWindow(img, processedColor)
        else:
            window_box.drawOpenCVRect(img)
        
        # for each window, how many new vehicles did we see?
        entrants.append(trackVehicles(window_box.vehicles_old_frames, results))            
        
        # add the latest frame to the old vehicle list
        window_box.vehicles_old_frames.append(results)
        
        # if the old vehicle list goes far enough back, drop the oldest set of vehicles
        if len(window_box.vehicles_old_frames) > VEHICLE_HISTORY_LOOKBACK:
            del window_box.vehicles_old_frames[0]
            
    return img, entrants

