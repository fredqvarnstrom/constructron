# Copyright ConstrucTRON Inc, 2016

# MOG Background subtraction motion detection from m-jpeg stream of IP cam


# import the necessary packages
import time
import cv2
import numpy as np

# Setup the Background Subtractor
# MOG1 no longer exists - http://code.opencv.org/issues/4367
pMOG = cv2.createBackgroundSubtractorMOG2()
# Build the erosion and dialation elements
erodeElement = cv2.getStructuringElement(cv2.MORPH_RECT, (8,8))
dilateElement = cv2.getStructuringElement(cv2.MORPH_RECT, (4,4))
# Minimum threshold for motion processing 
SENSITIVITY_VALUE = 20
# If number of objects in a frame is greater than MAX_NUM_OBJECTS we have a noisy filter
MAX_NUM_OBJECTS = 50
# Smallest acceptable contour (noise threshold)
MIN_OBJECT_AREA = 4
 
# allow the camera to warmup
time.sleep(0.1)

def processFrame(frame):
    # Background subtraction
    foregroundMask = pMOG.apply(frame);
    
    # Blur filter - reduce noise
    blurred = cv2.blur(foregroundMask, (20,20))

    # double erode - grow white areas twice (why?)
    eroded = cv2.erode(cv2.erode(blurred, erodeElement), erodeElement)

    # double dilate = shrink white areas twice (why again?)
    dilated = cv2.dilate(cv2.dilate(eroded, dilateElement), dilateElement)

    # threshold - get rid of pixels with near-zero values
    thresholded = cv2.threshold(dilated, SENSITIVITY_VALUE, 255, cv2.THRESH_BINARY)[1]
    return thresholded

def searchForMovement(processedFrame):
    # find contours screws with the input matrix, so make a copy
    temp = np.copy(processedFrame)

    # find the contours (shapes) in the processed image
    _, contours, hierarchy = cv2.findContours(temp, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # initialize return list
    vehicles = []


    # This is a really weird way of iterating through the contours.  Especially since RETR_EXTERNAL
    #   removes all nested contours
    if hierarchy is not None and hierarchy.shape[1] <= MAX_NUM_OBJECTS:
        index = 0
        while index >= 0:

            # ??? what about contourArea() ?
            moment = cv2.moments(contours[index])
            area = moment['m00']

            if area > MIN_OBJECT_AREA:
                vehicles.append((moment['m10']/area, moment['m01']/area, area))

            # goto next contour in hierarchy
            index = hierarchy[0][index][0]

    return vehicles
    
while(True):

    cap = cv2.VideoCapture("http://192.168.1.13/snap.jpg")

    if cap.isOpened():
        ret,img = cap.read()
        
        # Process the frame
        processed = processFrame(img)
        
        # Look for the vehicles
        results = searchForMovement(processed)
        
        # for now, just draw circles
        processed = cv2.cvtColor(processed, cv2.COLOR_GRAY2RGBA)# need color
        for vehicle in results:
            cv2.circle(processed, (int(vehicle[0]), int(vehicle[1])), 10, (0, 255, 0, 255), -1)
            
        cv2.imshow("win",processed)
		
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break    
    
    


