# Copyright ConstrucTRON Inc, 2016

import cv2
import numpy as np
from tkinter import *
import tkinter.messagebox
from PIL import Image, ImageTk
import WindowBox
import VehicleMOG

class IPcam(Frame):
	
	def __init__(self, master, mode = 'Raw', debugMode = 'Normal'):

		# Super constructor
		Frame.__init__(self, master, background="white")

		# Make the whole window a canvas
		self.imageCanvas = Canvas(self)
		self.imageCanvas.grid()
	
		self.listening = True
		
		self.windows = []
		self.max_windows = 3
		self.windowColors = ["magenta", "cyan", "yellow"]
		
		
		self.mode = mode
		
		self.debugMode = debugMode
		
		# mouse bindings
		self.imageCanvas.bind("<Button-1>", self.mouseDown)
		self.imageCanvas.bind("<Button-3>", self.rightMouseDown)
		self.imageCanvas.bind("<B1-Motion>", self.mouseDrag)
		self.imageCanvas.bind("<ButtonRelease-1>", self.mouseUp)

		self.windowDrag = None
		
		self.MOG = VehicleMOG.VehicleMOG()
		
	def mouseDown(self, event):
		self.windowStart = event
		
	def rightMouseDown(self, event):
		self.windows[:] = [window for window in self.windows if not window.clickInside(event.x,event.y)]
		
	def mouseDrag(self, event):
		# start window drag
		self.imageCanvas.delete(self.windowDrag)
		self.windowDrag = self.imageCanvas.create_rectangle(self.windowStart.x,self.windowStart.y,event.x, event.y, outline='cyan', width = 5)
		
	def mouseUp(self, event):
		self.windowdrag = False
		
		# degenerate windows
		if self.windowStart.x == event.x:
			return
		if self.windowStart.y == event.y:
			return
			
		# swap points if necessary
		if self.windowStart.x > event.x:
			self.windowStart.x, event.x = event.x, self.windowStart.x
		if self.windowStart.y > event.y:
			self.windowStart.y, event.y = event.y, self.windowStart.y
		
		# top, bottom, left, right
		window = WindowBox.WindowBox(self.windowStart.y, event.y, self.windowStart.x, event.x,  "cyan")        
		
		print(window.printString())
		
		self.addWindow(window)
		
	def setMode(self, mode):
		print("Switching to ", mode, " mode")
		self.mode = mode
		
	def setDebugMode(self, debugMode):
		print("Switching to ", debugMode, " debug mode")
		self.debugMode = debugMode
		
	def addWindow(self, window):
		
		# add the latest window to the window list
		self.windows.append(window)
		
		# if the window list is already at the max, drop the oldest window
		if len(self.windows) > self.max_windows:
			del self.windows[0]

	def listen(self, cam_ip):
		
		self.listening = True
		
		# loop until interrupted
		while(self.listening):

			cap = cv2.VideoCapture(cam_ip)

			if cap.isOpened():
				ret,img = cap.read()
				
				if img != []:
					
					# rearrange color channel
					b,g,r = cv2.split(img)
					img = cv2.merge((r,g,b))
					
					# "Raw" mode means dump the raw IP Cam frame - no processing
					if self.mode != "Raw":
						img, entrants = self.MOG.doMOG(img, self.windows, self.mode == "Debug", self.debugMode)      
						if self.mode == 'Detect': 
							for windowPosition in range(len(self.windows)):
								self.windows[windowPosition].addCount(entrants[windowPosition])
									 
					
					# Turn the numpy array into an image
					image = Image.fromarray(img)
			
					# display on the canvas
					imagePhoto = ImageTk.PhotoImage(image)

					self.imageCanvas.create_image((img.shape[1]/2,img.shape[0]/2),image=imagePhoto)
					
					#dump the image?
					img = []
				
			else:
				print("Open IP cam failed.", cam_ip)
				tkinter.messagebox.showwarning("Warning","Could not open IP Cam\n(%s)" % cam_ip)
				self.listening = False

	def resetCount(self):
		for window in self.windows:
			window.setCount(0)
			
				
	# interrupt
	def stopListening(self):
		self.listening = False
		
	def setErodeElement(self, erodeElementSize):
		self.MOG.setErodeElement(erodeElementSize)

	def setDilateElement(self, dilateElementSize):
		self.MOG.setDilateElement(dilateElementSize)

	def setSensitivityThreshold(self, sensitivityThreshold):
		self.MOG.setSensitivityThreshold(sensitivityThreshold)

	def setMinimumObjectArea(self, smallestContour):
		self.MOG.setMinimumObjectArea(smallestContour)

	def setTopBuffer(self, entryBandSize):
		self.MOG.setTopBuffer(entryBandSize)

	def setMinNewVehicleDistance(self, vehicleDistance):
		self.MOG.setMinNewVehicleDistance(vehicleDistance)
		
		
		
