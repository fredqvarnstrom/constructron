# Copyright ConstrucTRON Inc, 2016

from tkinter import *
import tkinter.simpledialog
import cv2
import numpy as np
import IPcam
import threading
import VehicleMOG

class App:

    def __init__(self, master):
        
        self.cam_ip = "http://192.168.1.15/snap.jpg"

        frame = Frame(master)
        frame.pack()
        
        buttonBar = Frame(frame)
        buttonBar.pack()

        MODES = ["Raw","Detect","Debug"]

        modeValue = StringVar()
        modeValue.set(MODES[0])
        

        for title in MODES:
            Radiobutton(buttonBar, indicatoron=0, text=title, variable=modeValue, value=title, command=lambda: self.setMode(modeValue.get())).pack(side=LEFT)
        
        Button(buttonBar, text="Load", command=self.get_camIP).pack(side=LEFT)
        Button(buttonBar, text="Pause", command=self.stopListening).pack(side=LEFT)
        Button(buttonBar, text="Reset", command=self.resetCount).pack(side=LEFT)
        Button(buttonBar, text="Quit", command= frame.quit).pack(side=LEFT)
        
        # Setup the debug choices bar
        self.debugBar = Frame(frame)
        self.debugBar.pack()
        # hide it, at first
        self.debugBar.pack_forget()
        
        # Which debug output should we show?
        DEBUG_MODES = ["MOG", "Eroded","Dialted","Thresholded", "Normal"]        
        debugValue = StringVar()
        debugValue.set(DEBUG_MODES[4])        

        for title in DEBUG_MODES:
            Radiobutton(self.debugBar, indicatoron=0, text=title, variable=debugValue, value=title, command=lambda: self.ipCam.setDebugMode(debugValue.get())).pack(side=LEFT)

        # Add the video window
        self.ipCam = IPcam.IPcam(frame, modeValue.get(), debugValue.get())
        self.ipCam.pack()
                
        #  parameters:
        parameterBar = Frame(frame)
        parameterBar.pack()
        
        # Erode element size
        erodeSize = IntVar()
        erodeSize.set(4)
        Scale(parameterBar, label='Erode', to_=8, variable=erodeSize, orient=HORIZONTAL, resolution=2, command=lambda _: self.setErodeSize(erodeSize.get())).pack(side=LEFT)
        
        # Dilate element size
        dilateSize = IntVar()
        dilateSize.set(4)
        Scale(parameterBar, label='Dilate', to_=8, variable=dilateSize, orient=HORIZONTAL, resolution=2, command=lambda _: self.setDilateSize(dilateSize.get())).pack(side=LEFT)    
            
        # Thresholding value
        threshSize = IntVar()
        threshSize.set(20)
        Scale(parameterBar, label='Threshold', to_=50, variable=threshSize, orient=HORIZONTAL, resolution=5, command=lambda _: self.setThreshSize(threshSize.get())).pack(side=LEFT)    
         
        # Detection settings 
        # Miniumum contor size (to be a vehicle)
        contourSize = IntVar()
        contourSize.set(100)
        Scale(parameterBar, label='Contour', to_=200, variable=contourSize, orient=HORIZONTAL, resolution=25, command=lambda _: self.setContourSize(contourSize.get())).pack(side=LEFT)    
                
        # Top threshold height ("band"/starting line - when a vehichle crosses the line, it "enters" the box)
        enterSize = IntVar()
        enterSize.set(40)
        Scale(parameterBar, label='Entry width', to_=100, from_=10, variable=enterSize, orient=HORIZONTAL, resolution=10, command=lambda _: self.setEnterSize(enterSize.get())).pack(side=LEFT)    
         
        # Minimum vehicle distance for a "new" vehicle entry (sometimes a single vehicle is seen twice within the starting band in sucessive (or near-sucessive) frames)
        newVehicleDist = IntVar()
        newVehicleDist.set(20)
        Scale(parameterBar, label='New Vehicle Dist', to_=60, variable=newVehicleDist, orient=HORIZONTAL, resolution=5, command=lambda _: self.setVehicleDist(newVehicleDist.get())).pack(side=LEFT)    
      
    def setErodeSize(self, erodeValue):
        print("Set erode size to ", erodeValue)
        self.ipCam.setErodeElement(erodeValue)
        
    def setDilateSize(self, dilateValue):
        print("Set dilate size to ", dilateValue)
        self.ipCam.setDilateElement(dilateValue)
        
    def setThreshSize(self, threshValue):
        print("Set threshold size to ", threshValue)
        self.ipCam.setSensitivityThreshold(threshValue)
        
    def setContourSize(self, contourValue):
        print("Set minimum contour size to ", contourValue)        
        self.ipCam.setMinimumObjectArea(contourValue)

    def setEnterSize(self, entryValue):
        print("Set entry band size to ", entryValue)
        self.ipCam.setTopBuffer(entryValue)
        
    def setVehicleDist(self, vehicleDist):
        print("Set minimum new vehcile distance to ", vehicleDist)
        self.ipCam.setMinNewVehicleDistance(vehicleDist)

    def get_camIP(self):
        
        ipString = tkinter.simpledialog.askstring("IP Cam Address", "IP Address:", initialvalue=self.cam_ip)
        
        # Run the IP cam viewer in a background thread
        t = threading.Thread(target=self.ipCam.listen, args=(ipString,))
        # because this is a daemon, it will die when the main window dies
        t.setDaemon(True)
        t.start()
        
    def setMode(self, mode):
        self.ipCam.setMode(mode)
        if mode == 'Debug':
            self.debugBar.pack()
        else:
            self.debugBar.pack_forget()
        
    def stopListening(self):
        print("Stop listening")
        self.ipCam.stopListening()
        
    def resetCount(self):
        self.ipCam.resetCount()
            
root = Tk()
app = App(root)
root.mainloop()
