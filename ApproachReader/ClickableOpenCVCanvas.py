from tkinter import *
from OpenCVCanvas import OpenCVCanvas
from WindowBox2 import WindowBox
import cv2

class ClickableOpenCVCanvas(OpenCVCanvas):
	
	def __init__(self, args, **kwargs):
		super().__init__(args, **kwargs)
		
		self.windows = []
		self.max_windows = 3
				
		self.windowColors = ["magenta", "cyan", "yellow"]
		self.currentColor = 0;        
		
		# mouse bindings
		self.bind("<Button-1>", self.mouseDown)
		self.bind("<Button-3>", self.rightMouseDown)
		self.bind("<B1-Motion>", self.mouseDrag)
		self.bind("<ButtonRelease-1>", self.mouseUp)
		
		self.windowDrag = None
	
	def mouseDown(self, event):
		self.windowStart = event
		
	def rightMouseDown(self, event):
		
		for window in self.windows:
			if window[0].clickInside(event.x,event.y):
				self.delete(window[1])
		
		self.windows[:] = [window for window in self.windows if not window[0].clickInside(event.x,event.y)]
		
		
	def mouseDrag(self, event):
		# start window drag
		self.delete(self.windowDrag)
		self.windowDrag = self.create_rectangle(self.windowStart.x,self.windowStart.y,event.x, event.y, outline=self.windowColors[self.currentColor], width = 5)
		
	def mouseUp(self, event):
		
		# degenerate windows
		if self.windowStart.x == event.x:
			return
		if self.windowStart.y == event.y:
			return
			
		# swap points if necessary
		if self.windowStart.x > event.x:
			self.windowStart.x, event.x = event.x, self.windowStart.x
		if self.windowStart.y > event.y:
			self.windowStart.y, event.y = event.y, self.windowStart.y
		
		# top, bottom, left, right
		window = WindowBox(self.windowStart.y, event.y, self.windowStart.x, event.x,  self.windowColors[self.currentColor])    
		self.currentColor = (self.currentColor + 1) % 3
				
		self.addWindow(window)
				
	def addWindow(self, window):
		
		# add the latest window to the window list
		outline = self.windowDrag
		self.windowDrag = None
		self.windows.append([window, outline])
		
		# if the window list is already at the max, drop the oldest window
		if len(self.windows) > self.max_windows:
			self.delete(self.windows[0][1])
			del self.windows[0]		
			
	def getWindowBoxes(self):
		if not self.windows:
			return []
		return [x[0] for x in self.windows]
		
	# Display numpy array as image on GUI			
	def publishArray(self, numpyArray, counting):		
		
		# OpenCV reads in BGR - switch to RGB
		numpyArray = cv2.cvtColor(numpyArray, cv2.COLOR_BGR2RGB)
		
		if counting:
			for window in self.windows:
				window[0].drawCount(numpyArray)
		
		OpenCVCanvas.publishArray(self, numpyArray)
		self.tag_lower(self.canvasImage)		

		
		
		
		
		
