# Copyright ConstrucTRON Inc, 2016

# import the necessary packages
import time
import cv2
import numpy as np
import sys   
import Vehicle
import WindowBox   
		
class VehicleMOG:

	def __init__(self):
		
		# MOG not playing nice with OpenCL
		# See https://github.com/opencv/opencv/issues/6055
		# Recompile OpenCV without OpenCL support
				
		# Setup the Background Subtractor
		# MOG1 no longer exists - http://code.opencv.org/issues/4367
		self.pMOG = cv2.createBackgroundSubtractorMOG2()

		# Build the erosion and dialation elements
		self.erodeElement = cv2.getStructuringElement(cv2.MORPH_RECT, (4,4))
		self.doErode = True
	
		# These should move with changes in resolution (pixels/vehicle)
		self.dilateElement = cv2.getStructuringElement(cv2.MORPH_RECT, (4,4))
		self.doDilate = True
		
		# Minimum threshold for motion processing 
		self.SENSITIVITY_VALUE = 20
		
		# If number of objects in a frame is greater than MAX_NUM_OBJECTS we have a noisy filter
		self.MAX_NUM_OBJECTS = 50
		
		# Smallest acceptable contour (noise threshold)
		self.MIN_OBJECT_AREA = 100
			
		# When counting entrants into a box, how tall should the entrant window be?
		self.TOP_BUFFER = 40
	
		# When counting entrants into a box, how far away does the centroid have to be to count as a new vehicle?
		self.MIN_NEW_VEHICLE_DISTANCE = 20
		
		# how many frames back should we check vehicle history?
		self.VEHICLE_HISTORY_LOOKBACK = 2
		
	def setErodeElement(self, erodeElementSize):
		print('set erode element size to',erodeElementSize)
		if erodeElementSize > 0:
			self.erodeElement = cv2.getStructuringElement(cv2.MORPH_RECT, (erodeElementSize,erodeElementSize))
			self.doErode = True
		else:
			self.doErode = False

	def setDilateElement(self, dilateElementSize):
		if dilateElementSize > 0:
			self.dilateElement = cv2.getStructuringElement(cv2.MORPH_RECT, (dilateElementSize,dilateElementSize))
			self.doDilate = True
		else:
			self.doDilate = False
	
	def setSensitivityThreshold(self, sensitivityThreshold):
		self.SENSITIVITY_VALUE = sensitivityThreshold
			
	def setMinimumObjectArea(self, smallestContour):
		self.MIN_OBJECT_AREA = smallestContour

	def setTopBuffer(self, entryBandSize):
		self.TOP_BUFFER = entryBandSize
		
	def setMinNewVehicleDistance(self, vehicleDistance):
		self.MIN_NEW_VEHICLE_DISTANCE = vehicleDistance
	
	# Try and isolate vehicles as white blobs
	def processFrame(self, frame, mode='Normal'):
		
		# Blur filter - reduce noise
		#blurred = cv2.blur(foregroundMask, (20,20))

		# shrink morph
		if self.doErode:
			eroded = cv2.erode(frame, self.erodeElement)        
		else:
			eroded = frame
		
		if mode == 'Eroded':
			return eroded

		# grow morph
		dilated = cv2.dilate(eroded, self.dilateElement)
		
		if mode == 'Dilated':
			return dilated

		# shrink/grow is a good way to get rid of noise, without softening edges like a filter

		# threshold - get rid of pixels with near-zero values
		thresholded = cv2.threshold(eroded, self.SENSITIVITY_VALUE, 255, cv2.THRESH_BINARY)[1]
		
		return thresholded

	# Using contour to turn the white blobs into shapes - we'll track these shapes by their centroid
	def searchForMovement(self, processedFrame):
		# find contours screws with the input matrix, so make a copy
		temp = np.copy(processedFrame)

		# find the contours (shapes) in the processed image
		_, contours, hierarchy = cv2.findContours(temp, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

		# initialize return list
		vehicles = []

		# This is a really weird way of iterating through the contours.  Especially since RETR_EXTERNAL
		#   removes all nested contours
		if hierarchy is not None and hierarchy.shape[1] <= self.MAX_NUM_OBJECTS:
			index = 0
			while index >= 0:

				# ??? what about contourArea() ?
				moment = cv2.moments(contours[index])
				area = moment['m00']

				# throw out small contours as noise
				if area > self.MIN_OBJECT_AREA:
					# center the vehicle marker at the contour's center of mass
					vehicle = Vehicle.Vehicle(moment['m10']/area, moment['m01']/area)
					vehicles.append(vehicle)

				# goto next contour in hierarchy
				index = hierarchy[0][index][0]

		return vehicles

	# When does a vehicle enter the box?  (from the top)
	def trackVehicles(self, vehicles_old_frames, vehicles_new):
		count = 0
		for vehicle in vehicles_new: 
			# If it just crosses a line, noise and sample size will cause misses.
			# So let's use a stripe of height "TOP_BUFFER" as our trigger "line"
			# If we find a vehicle centeroid in the TOP_BUFFER, it's an entrant       
			if vehicle.yPos < self.TOP_BUFFER:
				for frame in vehicles_old_frames:                
					for vehicle_old in frame:
						# However, a vehicle might get counted twice if it doesn't leave the buffer stripe
						# area before the next frame.  So we track the last few frames, and if the vehicle is too close
						# to a vehicle from a recent frame, consider it a potential double-count and ignore it
						if vehicle.rDist(vehicle_old) < self.MIN_NEW_VEHICLE_DISTANCE:
							break
					else:
						continue
					break
				else:                    
					count += 1
		return count

	# main MOG function - give me a picture a list of subwindow, and a display flag
	def doMOG(self, img, window_boxes, showProcessFlag, processMode='Normal'):		
		
		# Background subtraction
		foregroundMask = self.pMOG.apply(img);
		# Can't double up the MOG (i.e. can't apply twice on separate windows
		# if this gets hairy, we could min/max all the subwindows and only MOG that
		
		entrants = []
		
		for window_box in window_boxes:
			
			window = window_box.getSubimg(foregroundMask)
			
			# Process the frame
			if processMode != 'MOG':
				processed = self.processFrame(window, processMode)
			else:
				processed = window
			
			# Look for the vehicles (only for "normal" processing - non-debug mode is always "normal")
			if processMode == 'Normal' or not showProcessFlag:
				results = self.searchForMovement(processed)	
			
			# debug mode
			if showProcessFlag:
				# for now, just draw circles
				processedColor = cv2.cvtColor(processed, cv2.COLOR_GRAY2RGB)# need color
				
				if processMode == 'Normal':            
					for vehicle in results:
						cv2.circle(processedColor, (int(vehicle.xPos), int(vehicle.yPos)), 10, (0, 255, 0, 255), -1)        
						
				window_box.publishToWindow(img, processedColor)
			else:
				window_box.drawOpenCVRect(img)
			
			if processMode == 'Normal' or not showProcessFlag:

				# for each window, how many new vehicles did we see?
				entrants.append(self.trackVehicles(window_box.vehicles_old_frames, results))            
				
				# add the latest frame to the old vehicle list
				window_box.vehicles_old_frames.append(results)
			
				# if the old vehicle list goes far enough back, drop the oldest set of vehicles
				if len(window_box.vehicles_old_frames) > self.VEHICLE_HISTORY_LOOKBACK:
					del window_box.vehicles_old_frames[0]
				
		return img, entrants

