# Copyright ConstrucTRON Inc, 2016

import numpy as np

# simple struct to represent a vehicle's position
class Vehicle:
    def __init__(self, x, y):
        self.xPos = x
        self.yPos = y
    
    # Euclidean distance
    def dist(self, other):
        return np.linalg.norm(np.array([self.xPos, self.yPos])-np.array([other.xPos, other.yPos]))
    
    # Faster distance function - rectangular distance  
    def rDist(self, other):
        return abs(self.xPos - other.xPos) + abs(self.yPos - other.yPos)
